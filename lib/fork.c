// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW		0x800

//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	int r;

	// Check that the faulting access was (1) a write, and (2) to a
	// copy-on-write page.  If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).
	uintptr_t addr_va = (uintptr_t)addr;
	envid_t envid = sys_getenvid();
	pte_t pte = uvpt[addr_va >> 12];
	// LAB 4: Your code here.
	if((err & FEC_WR) == 0 || (pte & PTE_COW) == 0)
	{
		panic("pg_fault, no write or copy-on-write page");
	}
	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then move the new
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.
	if(sys_page_alloc(envid,(void*)PFTEMP,PTE_W | PTE_U | PTE_P) < 0)
	{
		panic("pgfault, 1");
	}
	memcpy((void*)PFTEMP,(void*)((addr_va >> 12) << 12),PGSIZE);
	if(sys_page_map(envid,(void*)PFTEMP,envid,(void*)((addr_va >> 12) << 12),PTE_U | PTE_W | PTE_P) < 0)
	{
		panic("pgfault, 2");
	}
	if(sys_page_unmap(envid,(void*)PFTEMP) < 0)
	{
		panic("pgfault, 3");
	}
	// LAB 4: Your code here.

}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address.  If the page is writable or copy-on-write,
// the new mapping must be created copy-on-write, and then our mapping must be
// marked copy-on-write as well.  (Exercise: Why do we need to mark ours
// copy-on-write again if it was already copy-on-write at the beginning of
// this function?)
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int
duppage(envid_t envid, unsigned pn)
{
	uintptr_t pg_addr = pn * PGSIZE;
	envid_t parent_envid = thisenv->env_id;
	if(!(uvpt[pn] & PTE_P))
	{
		panic("duppage, PTE_P");
	}
	if(uvpt[pn] & PTE_W || uvpt[pn] & PTE_COW)
	{
		if(sys_page_map(parent_envid,(void*)pg_addr,envid,(void*)pg_addr,PTE_P| PTE_U | PTE_COW) < 0)
		{
			panic("duppage, first sys_page_map");
		}
		if(sys_page_map(parent_envid,(void*)pg_addr,parent_envid,(void*)pg_addr,PTE_U | PTE_P | PTE_COW) < 0)
		{
			panic("duppage, second sys_page_map");
		}
	}
	else
	{
		if(sys_page_map(parent_envid,(void*)pg_addr,envid,(void*)pg_addr,PTE_P | PTE_U) < 0)
		{
			panic("duppage, sys_page_map read");
		}
	}
	return 0;
}

//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
	// LAB 4: Your code here.
	set_pgfault_handler(pgfault);
	int exofork_ret;
	if((exofork_ret = sys_exofork()) < 0)
	{
		panic("fork, sys_exofork error");
	}
	if(exofork_ret > 0)
	{
		size_t pg = 0;
		for(;pg < USTACKTOP;pg+=PGSIZE)
		{
			if(uvpd[pg >> 22] & PTE_P && uvpt[pg >> 12] & PTE_P)
			{
				duppage(exofork_ret,pg/PGSIZE);
			}
		}
		if(sys_page_alloc(exofork_ret,(void*)(UXSTACKTOP - PGSIZE),PTE_W | PTE_U | PTE_P) < 0)
		{
			panic("fork, parent sys_page_alloc");
		}
		if(sys_env_set_pgfault_upcall(exofork_ret,thisenv->env_pgfault_upcall) < 0)
		{
			panic("fork, parent sys_env_set_pgfault_upcall");
		}
		if(sys_env_set_status(exofork_ret,ENV_RUNNABLE) < 0)
		{
			panic("fork, parent sys_env_set_status");
		}
	}
	else
	{
		thisenv = &envs[ENVX(sys_getenvid())];
	}
	return exofork_ret;
}

// Challenge!
int
sfork(void)
{
	panic("sfork not implemented");
	return -E_INVAL;
}
